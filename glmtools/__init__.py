#!/usr/bin/python

# vim: set expandtab ts=4 sw=4:

from . import fit  # noqa F401
from . import design  # noqa F401
from . import data  # noqa F401
from . import permutations  # noqa F401
from . import example  # noqa F401
from . import viz  # noqa F401
from . import stats  # noqa F401
from . import regressors  # noqa F401
from . import util  # noqa F401

__version__ = '0.2.0'
