
import unittest

import numpy as np
from glmtools import data, design


class AbstractAPITest(unittest.TestCase):
    def make_data_twocat(self):

        # Make a fake dataset
        condition = np.tile([1, 2], 4)
        covariate = np.array([-3, -2, -1, 0, 0, 1, 2, 3])
        session = np.repeat([1, 2, 3, 4], 2)

        # Some random data
        X = np.random.randn(8,)

        self.glmdata = data.TrialGLMData(data=X,
                                         category_list=condition,
                                         covariate=covariate,
                                         session=session)

    def make_data_anova(self):

        # Make a fake dataset
        condition = np.tile([1, 2, 3, 4], 2)
        covariate = np.array([-3, -2, -1, 0, 0, 1, 2, 3])

        # Some random data
        X = np.random.randn(8,)

        self.glmdata = data.TrialGLMData(data=X,
                                         category_list=condition,
                                         covariate=covariate)


class test_DesignConfig_ObjAPI_TwoCat(AbstractAPITest):

    def setUp(self):
        self.make_data_twocat()

    def test_TwoCategorical_designconfig(self):
        # Make design config with Two categorical regressors
        DC = design.DesignConfig()
        DC.add_regressor(name='Cond1', rtype='Categorical', codes=1)
        DC.add_regressor(name='Cond2', rtype='Categorical', codes=2)
        DC.add_simple_contrasts()

        assert(len(DC.regressors) == 2)
        assert(len(DC.contrasts) == 2)

        DC.add_contrast(name='CondDiff', values=[1, -1])
        assert(len(DC.contrasts) == 3)
        assert(DC.contrasts[2]['values'] == [1, -1])

        des = DC.design_from_datainfo(self.glmdata.info)
        assert(np.all(des.design_matrix[:, 1] == [0, 1, 0, 1, 0, 1, 0, 1]))
        assert(np.all(des.design_matrix[:, 0] == [1, 0, 1, 0, 1, 0, 1, 0]))

    def test_ConstParametric_designconfig(self):

        # Constant + parametric design
        DC = design.DesignConfig()
        DC.add_regressor(name='Const', rtype='Constant')
        DC.add_regressor(name='Cond2', rtype='Categorical', codes=1, preproc='z')
        DC.add_simple_contrasts()

        des = DC.design_from_datainfo(self.glmdata.info)
        assert(np.all(des.design_matrix[:, 0] == [1, 1, 1, 1, 1, 1, 1, 1]))
        assert(np.all(des.design_matrix[:, 1] == [1, -1, 1, -1, 1, -1, 1, -1]))


class test_DesignConfig_ObjAPI_Anova(AbstractAPITest):

    def setUp(self):
        self.make_data_anova()

    def test_Anova_designconfig(self):

        DC = design.DesignConfig()
        DC.add_regressor(name='Cond1', rtype='Categorical', codes=1)
        DC.add_regressor(name='Cond2', rtype='Categorical', codes=2)
        DC.add_regressor(name='Cond3', rtype='Categorical', codes=3)
        DC.add_regressor(name='Cond4', rtype='Categorical', codes=4)

        assert(len(DC.regressors) == 4)

        DC.add_simple_contrasts()
        assert(len(DC.contrasts) == 4)

        DC.add_contrast(name='MainEffect1', values=[1, 1, -1, -1])
        DC.add_contrast(name='MainEffect2', values=[1, -1, 1, -1])
        DC.add_contrast(name='Interaction', values=[1, -1, -1, 1])

        assert(len(DC.contrasts) == 7)

        DC.add_ftest(name='MainEffect1', values=[1, 1, -1, -1])
        DC.add_ftest(name='MainEffect2', values=[1, -1, 1, -1])
        DC.add_ftest(name='Interaction', values=[1, -1, -1, 1])

        assert(len(DC.ftests) == 3)

        DC.add_regressor(name='Covariate', rtype='Parametric', datainfo='covariate')
        assert(len(DC.regressors) == 5)

        des = DC.design_from_datainfo(self.glmdata.info)
        assert(des.num_regressors == 5)


class test_DesignConfig_from_yaml(AbstractAPITest):

    def setUp(self):
        self.make_data_twocat()

    @property
    def yaml_text(self):
        return """
NewDesign:
  regressors:
  - {name: Cond1, rtype: Categorical, codes: 1}
  - {name: Cond2, rtype: Categorical, codes: 2}
  contrasts:
  - {name: Cond1, values: 1 0}
  - {name: Cond2, values: 0 1}
"""

    def test_TwoCategorical_from_yaml(self):

        DC = design.DesignConfig(yaml_text=self.yaml_text)
        assert(len(DC.regressors) == 2)
        assert(len(DC.contrasts) == 2)

        DC.add_contrast(name='CondDiff', values=[1, -1])
        assert(len(DC.contrasts) == 3)
        assert(DC.contrasts[2]['values'] == [1, -1])

        des = DC.design_from_datainfo(self.glmdata.info)
        assert(np.all(des.design_matrix[:, 1] == [0, 1, 0, 1, 0, 1, 0, 1]))
        assert(np.all(des.design_matrix[:, 0] == [1, 0, 1, 0, 1, 0, 1, 0]))
