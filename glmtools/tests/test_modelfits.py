
import unittest

import numpy as np


class test_betas(unittest.TestCase):

    def setUp(self):
        from glmtools import data, design, regressors

        # Simple model
        noise = .1
        Y = np.random.normal(size=(5000, 1))*noise + .5
        glmdat = data.ContinuousGLMData(data=Y, dim_labels=['time', 'trials'])

        regs = [regressors.ConstantRegressor(num_observations=glmdat.num_observations),
                regressors.TrendRegressor(num_observations=glmdat.num_observations, name='linear')]

        contrasts = [design.Contrast([1, 0], 'Mean'),
                     design.Contrast([0, 1], 'linear')]

        glmdes = design.GLMDesign.initialise(regs, contrasts)

        self.design = glmdes
        self.data = glmdat

        self.ndeci = 7

    def test_simple_glm_betas(self):
        from glmtools import fit

        glm_model = fit.OLSModel(self.design, self.data)
        np_beta, np_resid, np_rank, np_s = np.linalg.lstsq(self.design.design_matrix, self.data.data, rcond=None)

        # Check beta estimates are near identical
        assert(np.allclose(np_beta, glm_model.betas))

        # Check residual sum squares are near identical
        assert(np.allclose(np_resid, glm_model.ss_error))


class test_ttests(unittest.TestCase):

    def setUp(self):

        self.x1 = np.random.randn(24, 5)
        self.x2 = np.random.randn(24, 5)+1

        self.ndeci = 7

    def test_1sample_ttest(self):

        from glmtools.stats import ttest_1samp
        from scipy import stats

        t1 = ttest_1samp(self.x1)
        t2, p = stats.ttest_1samp(self.x1, 0)

        assert(np.all(np.round(t1, self.ndeci) == np.round(t2, self.ndeci)))

        t1 = ttest_1samp(self.x2)
        t2, p = stats.ttest_1samp(self.x2, 0)

        assert(np.all(np.round(t1, self.ndeci) == np.round(t2, self.ndeci)))

    def test_independant_ttest(self):

        from glmtools.stats import ttest_ind
        from scipy import stats

        t1 = ttest_ind(self.x1, self.x2)
        t2, p = stats.ttest_ind(self.x1, self.x2)

        assert(np.all(np.round(t1, self.ndeci) == np.round(t2, self.ndeci)))

        t1 = ttest_ind(self.x1, self.x2)
        t2, p = stats.ttest_ind(self.x1, self.x2)

        assert(np.all(np.round(t1, self.ndeci) == np.round(t2, self.ndeci)))

    def test_paired_ttest(self):

        from glmtools.stats import ttest_paired
        from scipy import stats

        t1 = ttest_paired(self.x1, self.x2)
        t2, p = stats.ttest_rel(self.x1, self.x2, axis=0)

        assert(np.all(np.round(t1, self.ndeci) == np.round(t2, self.ndeci)))

        t1 = ttest_paired(self.x1, self.x2)
        t2, p = stats.ttest_rel(self.x1, self.x2, axis=0)

        assert(np.all(np.round(t1, self.ndeci) == np.round(t2, self.ndeci)))


class test_ftests(unittest.TestCase):

    def setUp(self):

        self.conds = np.repeat(np.arange(4), 5)
        self.labels = np.repeat(('A', 'B', 'C', 'D'), 5)
        self.X = np.random.randn(*self.conds.shape)
        for ii in range(4):
            self.X[self.conds == ii] += ii

    def test_oneway_anova(self):

        from glmtools.stats import anova_1way
        from scipy import stats

        F = stats.f_oneway(self.X[self.conds == 0],
                           self.X[self.conds == 1],
                           self.X[self.conds == 2],
                           self.X[self.conds == 3])
        F2, des = anova_1way(self.X, self.conds)

        assert(np.allclose(F[0], F2))
