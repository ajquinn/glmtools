# vim: set noexpandtab ts=4 sw=4:
#

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

all: install
	python3 setup.py build

install:
	python3 setup.py install

clean:
	python3 setup.py clean
	rm -fr build
	rm -fr doc/build
	rm -fr emd.egg-info

all-clean: install-clean
	python3 setup.py build

install-clean: clean
	python3 setup.py install

test:
	python3 -m pytest glmtools

.PHONY: help Makefile
